Drupal.behaviors.loadDrushOutput = function () {
  $('#drush-ui-output pre').load(Drupal.settings.drushCommandPath);
  
  
  
};


Drupal.behaviors.drushFormAttach = function () {

  var commands = ['drush'];
  var commandCount = 0;
  var currentCommand = 0;
  
  $('#edit-command').keyup(function(event){
    if (event.keyCode == 38){
      //keyup
      if (currentCommand > 0){
        currentCommand--;
        $(this).val(commands[currentCommand]);
      }
    } else if (event.keyCode == 40){
      //keydown
      if (currentCommand <= commandCount){
        currentCommand++;
        $(this).val(commands[currentCommand]);
      }
    } else {
      commands[commandCount] = $(this).val();
    }
  });

    $('#drush-ui-form').submit(function(){
      var command = $('#edit-command').val();
      var url = Drupal.settings.drushCommandPath + '/' + escape($('#edit-command').val());
      
      commandCount++;
      currentCommand = commandCount;

      $('#edit-command').toggleClass('throbbing');
      $.get(url, {}, function(data){
        output = $('#edit-output').val();
        $('#edit-output').val(output + '\n' + $('#edit-command').val() + '\n' + data).scrollTop(10000000);
        $('#edit-command').val('drush ').toggleClass('throbbing');
      });
      commands[currentCommand] = $(this).val();
      console.log(commands);
      return false;
    });
    
    $('#drush-ui-form').submit();
};      